---

## CutieKeys


简介： 可以使用 VR 控制器，敲击虚拟键盘，输出字母。

调试难度： ★★★

基于CutieKeys，完成以下tasks：

1. 制作 VR 钢琴   （难度： ★）
2. 制作 VR 架子鼓 （难度： ★★）
3. 制作 VR 打地鼠 （难度： ★★★）

![CutieKeys](https://bitbucket.org/blueprintrealityinc/vr_demo_instruction/raw/master/demo1.png "快来愉快地开始你的第一个VR App吧")

---

Acknowledge

https://assetstore.unity.com/packages/templates/systems/steamvr-plugin-32647

https://github.com/NormalVR/CutieKeys